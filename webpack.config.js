const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: ''
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.html$/,
        exclude: /node_modules/,
        use: {loader: 'html-loader'}
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        loader: "file-loader"
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
    ]
  },
  devServer: {
    historyApiFallback: true,
  },
  plugins: [
    new HtmlWebpackPlugin({
      hash: true,
      filename: 'index.html',
      template: './src/index.html'
    }),
    new WorkboxPlugin.GenerateSW({
      swDest: 'service-worker.js'
    }),
    new WorkboxPlugin.InjectManifest({
      swSrc: './src/service-worker.js',
      swDest: 'service-worker.js'
    }),
    new WebpackPwaManifest({
      name: 'E-shop PWA',
      short_name: 'E-shop',
      description: 'Online store',
      background_color: '#01579b',
      theme_color: '#01579b',
      start_url: '/',
      icons: [
        {
          src: './src/assets/images/ninja-shirt.jpg',
          sizes: [96, 128, 256, 384, 512],
          destination: path.join('assets', 'icons')
        }
      ]
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css",
    })
  ],
  mode: 'development'
};
