/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/assets/images/ninja-shirt.jpg":
/*!*******************************************!*\
  !*** ./src/assets/images/ninja-shirt.jpg ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = __webpack_require__.p + \"6de226a6eb9417cbb88d3016fa908a86.jpg\";\n\n//# sourceURL=webpack:///./src/assets/images/ninja-shirt.jpg?");

/***/ }),

/***/ "./src/assets/images/ninja-sweater.jpg":
/*!*********************************************!*\
  !*** ./src/assets/images/ninja-sweater.jpg ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = __webpack_require__.p + \"0a2a6337df8eb3d65c63220942826e71.jpg\";\n\n//# sourceURL=webpack:///./src/assets/images/ninja-sweater.jpg?");

/***/ }),

/***/ "./src/components/addPage/add.html":
/*!*****************************************!*\
  !*** ./src/components/addPage/add.html ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = \"<main class=\\\"main-wrap\\\">\\r\\n  <div class=\\\"main-wrapper container\\\">\\r\\n    <section class=\\\"section-wrap box-shadow\\\">\\r\\n      <article class=\\\"card-title\\\">\\r\\n        <h2 class=\\\"page-title\\\"></h2>\\r\\n      </article>\\r\\n      <form>\\r\\n        <div class=\\\"inner-form\\\">\\r\\n          <label for=\\\"name\\\"><b>Name</b></label>\\r\\n          <input class=\\\"form-name\\\" type=\\\"text\\\" name=\\\"name\\\" placeholder=\\\"Item name\\\" required>\\r\\n\\r\\n          <label for=\\\"price\\\"><b>Price</b></label>\\r\\n          <input class=\\\"form-price\\\" type=\\\"number\\\" name=\\\"price\\\" placeholder=\\\"Item price\\\" required>\\r\\n\\r\\n          <label for=\\\"img\\\"><b>Image</b></label>\\r\\n          <select class=\\\"form-img\\\" name=\\\"image\\\" required>\\r\\n            <option value=\\\"ninja-shirt\\\">Ninja shirt</option>\\r\\n            <option value=\\\"ninja-sweater\\\">Ninja sweater</option>\\r\\n          </select>\\r\\n\\r\\n          <label for=\\\"sale\\\"><b>Item on sale?</b></label>\\r\\n          <input class=\\\"form-sale\\\" type=\\\"checkbox\\\" name=\\\"sale\\\" placeholder=\\\"Item on sale?\\\">\\r\\n\\r\\n          <label for=\\\"discount\\\"><b>Discount</b></label>\\r\\n          <input class=\\\"form-discount\\\" type=\\\"text\\\" name=\\\"discount\\\" placeholder=\\\"Discount amount\\\">\\r\\n\\r\\n          <button class=\\\"btn-form btn-add-item\\\" type=\\\"button\\\"></button>\\r\\n          <button class=\\\"btn-form btn-delete-item hidden\\\" type=\\\"button\\\">Delete item</button>\\r\\n        </div>\\r\\n      </form>\\r\\n    </section>\\r\\n  </div>\\r\\n</main>\";\n\n//# sourceURL=webpack:///./src/components/addPage/add.html?");

/***/ }),

/***/ "./src/components/addPage/add.js":
/*!***************************************!*\
  !*** ./src/components/addPage/add.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _add_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./add.html */ \"./src/components/addPage/add.html\");\n/* harmony import */ var _add_html__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_add_html__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _modules_shared__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../modules/shared */ \"./src/modules/shared.js\");\n/* harmony import */ var _modules_online_check__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../modules/online-check */ \"./src/modules/online-check.js\");\n/* harmony import */ var _modules_indexed_bd__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../modules/indexed-bd */ \"./src/modules/indexed-bd.js\");\nfunction _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\n\n\n\n\nvar shopObj = {\n  name: 'shop',\n  version: 1,\n  store: 'items'\n};\n\nfunction onSubmit(method, id) {\n  var validName = false;\n  var validPrice = false;\n  var validImage = false;\n  var item = {\n    name: '',\n    price: 0,\n    discount: null,\n    image: ''\n  };\n  Array.prototype.forEach.call(formElement(), function (input) {\n    if (Object.keys(item).includes(input.name)) {\n      if (input.name === 'price') {\n        item[input.name] = Number(input.value);\n\n        if (input.value.length > 0) {\n          validPrice = true;\n        }\n      } else if (input.name === 'discount') {\n        item[input.name] = document.querySelector('.form-sale').checked ? input.value : null;\n      } else {\n        item[input.name] = input.value;\n\n        if (input.name === 'name') {\n          if (input.value.length > 0) {\n            validName = true;\n          }\n        }\n\n        if (input.name === 'image') {\n          if (input.value.length !== 0) {\n            validImage = true;\n          }\n        }\n      }\n    }\n  });\n\n  if (!_modules_online_check__WEBPACK_IMPORTED_MODULE_2__[\"default\"].getConnectionState && id) {\n    _modules_indexed_bd__WEBPACK_IMPORTED_MODULE_3__[\"default\"].getLocalData(id, shopObj).then(function (data) {\n      data.onsuccess = function () {\n        item = _objectSpread({}, item, {\n          \"new\": data.result['new']\n        });\n\n        if (validName && validPrice && validImage) {\n          method === 'post' ? postItem(item) : putItem(item, id);\n        } else alert('Name, price, image fields must be filled...');\n      };\n    });\n  } else if (validName && validPrice && validImage) {\n    method === 'post' ? postItem(item) : putItem(item, id);\n  } else alert('Name, price, image fields must be filled...');\n}\n\nfunction onEditRender(content) {\n  var obj = content;\n  Array.prototype.forEach.call(formElement(), function (input) {\n    if (Object.keys(obj).includes(input.name)) {\n      input.value = obj[input.name];\n    }\n\n    if (obj.discount) {\n      document.querySelector('.form-sale').checked = true;\n    }\n  });\n  var btnDelete = document.querySelector('.btn-delete-item');\n  btnDelete.classList.remove('hidden');\n  btnDelete.addEventListener('click', function () {\n    deleteItem(btnDelete, obj.id);\n  });\n}\n\nfunction onEditFetch(id) {\n  if (_modules_online_check__WEBPACK_IMPORTED_MODULE_2__[\"default\"].getConnectionState) {\n    var url = \"http://localhost:3000/items?id=\".concat(id);\n    _modules_shared__WEBPACK_IMPORTED_MODULE_1__[\"default\"].fetchRequest(url, 'GET').then(function (content) {\n      return onEditRender(content[0]);\n    })[\"catch\"](function (err) {\n      return console.log(\"Item edit: \".concat(err));\n    });\n  } else {\n    _modules_indexed_bd__WEBPACK_IMPORTED_MODULE_3__[\"default\"].getLocalData(id, shopObj).then(function (data) {\n      data.onsuccess = function () {\n        console.log('Getting item from indexedDB');\n        onEditRender(data.result);\n      };\n    });\n  }\n}\n\nfunction deleteItem(el, id) {\n  if (_modules_online_check__WEBPACK_IMPORTED_MODULE_2__[\"default\"].getConnectionState) {\n    var url = \"http://localhost:3000/items/\".concat(id);\n    _modules_shared__WEBPACK_IMPORTED_MODULE_1__[\"default\"].fetchRequest(url, 'DELETE').then(function () {\n      el.classList.add('hidden');\n      alert(\"Item successfully deleted...\");\n      _modules_shared__WEBPACK_IMPORTED_MODULE_1__[\"default\"].changeLocation('/shop', '/shop');\n    })[\"catch\"](function (err) {\n      return console.log(\"Delete item: \".concat(err));\n    });\n  } else {\n    _modules_indexed_bd__WEBPACK_IMPORTED_MODULE_3__[\"default\"].deleteLocalData(id);\n    alert(\"Item successfully deleted...\");\n    el.classList.add('hidden');\n    _modules_shared__WEBPACK_IMPORTED_MODULE_1__[\"default\"].changeLocation('/shop', '/shop');\n  }\n}\n\nfunction putItem(obj, id) {\n  if (_modules_online_check__WEBPACK_IMPORTED_MODULE_2__[\"default\"].getConnectionState) {\n    var url = \"http://localhost:3000/items/\".concat(id);\n    _modules_shared__WEBPACK_IMPORTED_MODULE_1__[\"default\"].fetchRequest(url, 'PUT', obj).then(function (content) {\n      console.log(content);\n      alert('Item successfully edited...');\n    })[\"catch\"](function (err) {\n      return console.log(\"Put Item: \".concat(err));\n    });\n  } else {\n    _modules_indexed_bd__WEBPACK_IMPORTED_MODULE_3__[\"default\"].updateLocalData(obj, Number(id));\n  }\n}\n\nfunction postItem(obj) {\n  if (_modules_online_check__WEBPACK_IMPORTED_MODULE_2__[\"default\"].getConnectionState) {\n    var url = 'http://localhost:3000/items';\n    _modules_shared__WEBPACK_IMPORTED_MODULE_1__[\"default\"].fetchRequest(url, 'POST', obj).then(function (content) {\n      console.log(content);\n      alert('Item successfully added...'); // Clear form inputs\n\n      document.querySelector('.form-sale').checked = false;\n      Array.prototype.forEach.call(formElement(), function (input) {\n        input.type !== 'select-one' ? input.value = '' : null;\n      });\n    })[\"catch\"](function (err) {\n      return console.log(\"Post item: \".concat(err));\n    });\n  } else {\n    _modules_indexed_bd__WEBPACK_IMPORTED_MODULE_3__[\"default\"].addLocalData(obj); // Clear form inputs\n\n    document.querySelector('.form-sale').checked = false;\n    Array.prototype.forEach.call(formElement(), function (input) {\n      input.type !== 'select-one' ? input.value = '' : null;\n    });\n  }\n}\n\nfunction formElement() {\n  return [document.querySelector('.form-name'), document.querySelector('.form-price'), document.querySelector('.form-discount'), document.querySelector('.form-img')];\n}\n\nfunction formType() {\n  var pageTitle = document.querySelector('.page-title');\n  var btnAddEdit = document.querySelector('.btn-add-item');\n\n  if (history.state.Title === '/add-item') {\n    pageTitle.innerHTML = 'Add Item';\n    btnAddEdit.innerHTML = 'Add Item';\n  } else {\n    pageTitle.innerHTML = 'Edit Item';\n    btnAddEdit.innerHTML = 'Edit Item';\n  }\n}\n\nvar onLoadsubmitEvent = function onLoadsubmitEvent(id) {\n  formType();\n  var btnAddItem = document.querySelector('.btn-add-item');\n\n  if (id !== undefined && id) {\n    onEditFetch(id);\n    btnAddItem.addEventListener('click', function () {\n      onSubmit('put', id);\n    });\n  } else {\n    btnAddItem.addEventListener('click', function () {\n      onSubmit('post');\n    });\n  }\n};\n\nvar add = {\n  addComponent: _add_html__WEBPACK_IMPORTED_MODULE_0___default.a,\n  onLoadsubmitEvent: onLoadsubmitEvent\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (add);\n\n//# sourceURL=webpack:///./src/components/addPage/add.js?");

/***/ }),

/***/ "./src/components/headerPage/header.html":
/*!***********************************************!*\
  !*** ./src/components/headerPage/header.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = \"<header class=\\\"header\\\">\\r\\n  <nav class=\\\"nav\\\">\\r\\n    <div class=\\\"container\\\">\\r\\n      <div class=\\\"nav-wrapper\\\">\\r\\n        <div class=\\\"nav-element\\\">\\r\\n          <a class=\\\"btn-shop\\\">SHOP</a>\\r\\n        </div>\\r\\n        <div class=\\\"nav-element\\\">\\r\\n          <a class=\\\"btn-cart\\\">CART</a>\\r\\n        </div>\\r\\n      </div>\\r\\n    </div>\\r\\n  </nav>\\r\\n</header>\";\n\n//# sourceURL=webpack:///./src/components/headerPage/header.html?");

/***/ }),

/***/ "./src/components/headerPage/header.js":
/*!*********************************************!*\
  !*** ./src/components/headerPage/header.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _header_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header.html */ \"./src/components/headerPage/header.html\");\n/* harmony import */ var _header_html__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_header_html__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _modules_shared__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../modules/shared */ \"./src/modules/shared.js\");\n\n\n\nvar clickEvents = function clickEvents() {\n  var btnShop = document.querySelector('.btn-shop');\n  var btnCart = document.querySelector('.btn-cart');\n  btnShop.addEventListener('click', function () {\n    _modules_shared__WEBPACK_IMPORTED_MODULE_1__[\"default\"].changeLocation('shop', '/shop');\n  });\n  btnCart.addEventListener('click', function () {\n    _modules_shared__WEBPACK_IMPORTED_MODULE_1__[\"default\"].changeLocation('cart', '/cart');\n  });\n};\n\nvar header = {\n  headerComponent: _header_html__WEBPACK_IMPORTED_MODULE_0___default.a,\n  clickEvents: clickEvents\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (header);\n\n//# sourceURL=webpack:///./src/components/headerPage/header.js?");

/***/ }),

/***/ "./src/components/shopPage/shop.html":
/*!*******************************************!*\
  !*** ./src/components/shopPage/shop.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = \"<main class=\\\"main-wrap\\\">\\r\\n  <div class=\\\"main-wrapper container\\\">\\r\\n    <section class=\\\"section-wrap box-shadow\\\">\\r\\n      <article class=\\\"card-title\\\">\\r\\n        <h2>Shop</h2>\\r\\n        <h5>HOME/SHOP</h5>\\r\\n      </article>\\r\\n      <article class=\\\"card-sort\\\">\\r\\n        <div class=\\\"card-sort-content\\\">\\r\\n          <p>Showing 1-24 of 26 results</p>\\r\\n          <div>\\r\\n            <select name=\\\"\\\" id=\\\"\\\">\\r\\n              <option value=\\\"default\\\">DEFAULT SORTING</option>\\r\\n              <option value=\\\"price\\\">PRICE SORTING</option>\\r\\n            </select>\\r\\n          </div>\\r\\n        </div>\\r\\n      </article>\\r\\n      <article class=\\\"items\\\">\\r\\n        <!-- Generating products in shop.js -->\\r\\n      </article>\\r\\n      <div class=\\\"d-flex justify-end\\\">\\r\\n        <button class=\\\"btn-new\\\" type=\\\"button\\\" title=\\\"ADD\\\"></button>\\r\\n      </div>\\r\\n    </section>\\r\\n\\r\\n    <aside class=\\\"aside-wrap\\\">\\r\\n      <article class=\\\"side-item box-shadow\\\">\\r\\n        <div class=\\\"search-wrap\\\">\\r\\n          <input type=\\\"text\\\" placeholder=\\\"Search & hit enter...\\\">\\r\\n        </div>\\r\\n      </article>\\r\\n\\r\\n      <article class=\\\"side-item box-shadow\\\">\\r\\n        <h3>Recant Post</h3>\\r\\n        <div class=\\\"dot-line\\\"></div>\\r\\n        <ul class=\\\"list\\\">\\r\\n          <li>Something</li>\\r\\n          <li>Something else</li>\\r\\n          <li>Random</li>\\r\\n          <li>Random else</li>\\r\\n          <li>Something random else</li>\\r\\n        </ul>\\r\\n      </article>\\r\\n\\r\\n      <article class=\\\"side-item box-shadow\\\">\\r\\n        <h3>Recant Comments</h3>\\r\\n        <div class=\\\"dot-line\\\"></div>\\r\\n        <ul class=\\\"list\\\">\\r\\n          <li>Something</li>\\r\\n          <li>Something else</li>\\r\\n          <li>Random</li>\\r\\n          <li>Random else</li>\\r\\n          <li>Something random else</li>\\r\\n        </ul>\\r\\n      </article>\\r\\n\\r\\n      <article class=\\\"side-item box-shadow\\\">\\r\\n        <h3>Archive</h3>\\r\\n        <div class=\\\"dot-line\\\"></div>\\r\\n        <ul class=\\\"list\\\">\\r\\n          <li>Something 2019</li>\\r\\n        </ul>\\r\\n      </article>\\r\\n\\r\\n      <article class=\\\"side-item box-shadow\\\">\\r\\n        <h3>Categories</h3>\\r\\n        <div class=\\\"dot-line\\\"></div>\\r\\n        <ul class=\\\"list\\\">\\r\\n          <li>Category 1</li>\\r\\n          <li>Category 2</li>\\r\\n          <li>Category 3</li>\\r\\n          <li>Category 4</li>\\r\\n        </ul>\\r\\n      </article>\\r\\n    </aside>\\r\\n  </div>\\r\\n</main>\";\n\n//# sourceURL=webpack:///./src/components/shopPage/shop.html?");

/***/ }),

/***/ "./src/components/shopPage/shop.js":
/*!*****************************************!*\
  !*** ./src/components/shopPage/shop.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _shop_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./shop.html */ \"./src/components/shopPage/shop.html\");\n/* harmony import */ var _shop_html__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_shop_html__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _modules_shared__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../modules/shared */ \"./src/modules/shared.js\");\n/* harmony import */ var _modules_indexed_bd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../modules/indexed-bd */ \"./src/modules/indexed-bd.js\");\n/* harmony import */ var _modules_online_check__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../modules/online-check */ \"./src/modules/online-check.js\");\n/* harmony import */ var _assets_images_ninja_shirt_jpg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../assets/images/ninja-shirt.jpg */ \"./src/assets/images/ninja-shirt.jpg\");\n/* harmony import */ var _assets_images_ninja_shirt_jpg__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_assets_images_ninja_shirt_jpg__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _assets_images_ninja_sweater_jpg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../assets/images/ninja-sweater.jpg */ \"./src/assets/images/ninja-sweater.jpg\");\n/* harmony import */ var _assets_images_ninja_sweater_jpg__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_assets_images_ninja_sweater_jpg__WEBPACK_IMPORTED_MODULE_5__);\n\n\n\n\n\n\nvar shopObj = {\n  name: 'shop',\n  version: 1,\n  store: 'items'\n};\n\nfunction domItem(item) {\n  return \"\\n    <figure class=\\\"item \".concat(item.discount ? 'sale' : '', \"\\\">\\n      <img src=\\\"\").concat(selectImage(item.image), \"\\\" alt=\\\"happy ninja\\\" width=\\\"100%\\\">\\n      <button class=\\\"btn-edit\\\" data-item=\\\"\").concat(item.id, \"\\\" type=\\\"button\\\">EDIT</button>\\n      <div class=\\\"info\\\">\\n        <h4 class=\\\"truncate\\\">\").concat(item.name, \"</h4>\\n        <strong>\").concat(item.discount ? \"<span>\".concat((item.price + Number(item.discount)).toFixed(2), \"</span>\") : '', \" \").concat(item.price.toFixed(2), \"</strong>\\n        <button class=\\\"btn-add\\\" data-item=\\\"\").concat(item.id, \"\\\" type=\\\"button\\\">ADD TO CARD</button>\\n      </div>\\n    </figure>\\n  \");\n}\n\nfunction selectImage(img) {\n  switch (img) {\n    case 'ninja-shirt':\n      return _assets_images_ninja_shirt_jpg__WEBPACK_IMPORTED_MODULE_4___default.a;\n\n    case 'ninja-sweater':\n      return _assets_images_ninja_sweater_jpg__WEBPACK_IMPORTED_MODULE_5___default.a;\n  }\n}\n\nvar fetchItems = function fetchItems(query) {\n  if (_modules_online_check__WEBPACK_IMPORTED_MODULE_3__[\"default\"].getConnectionState) {\n    var url = \"http://localhost:3000/items\".concat(query);\n    _modules_shared__WEBPACK_IMPORTED_MODULE_1__[\"default\"].fetchRequest(url, 'GET').then(function (items) {\n      generateItems(items);\n      addEventToBtns();\n    })[\"catch\"](function (err) {\n      return console.log(\"Fetch items. Maybe offline?: \".concat(err));\n    });\n  } else {\n    _modules_indexed_bd__WEBPACK_IMPORTED_MODULE_2__[\"default\"].getLocalData(null, shopObj).then(function (data) {\n      data.onsuccess = function () {\n        console.log('Adding items form indexdeDB');\n        generateItems(data.result);\n        addEventToBtns();\n      };\n    });\n  }\n};\n\nfunction generateItems(items) {\n  var itemsElement = document.querySelector('.items');\n  Array.prototype.forEach.call(items, function (item) {\n    itemsElement.innerHTML += domItem(item);\n  });\n}\n\nfunction addEventToBtns() {\n  var btnsEdit = document.querySelectorAll('.btn-edit');\n  Array.prototype.forEach.call(btnsEdit, function (btn) {\n    btn.addEventListener('click', goToEdit);\n  });\n  var btnNew = document.querySelector('.btn-new');\n  btnNew.addEventListener('click', function () {\n    goToAddPage(undefined);\n  });\n  var btnsNodeList = document.querySelectorAll('.btn-add');\n  Array.prototype.forEach.call(btnsNodeList, function (btn) {\n    btn.addEventListener('click', addToCart);\n  });\n}\n\nfunction goToAddPage(id) {\n  id = id !== undefined ? _modules_shared__WEBPACK_IMPORTED_MODULE_1__[\"default\"].changeLocation('/edit-item', '/edit-item', id) : _modules_shared__WEBPACK_IMPORTED_MODULE_1__[\"default\"].changeLocation('/add-item', '/add-item');\n}\n\nfunction goToEdit(event) {\n  var id = event.target.dataset.item;\n  goToAddPage(id);\n}\n\nfunction addToCart(event) {\n  console.log(event.target.dataset.item);\n}\n\nvar shop = {\n  shopComponent: _shop_html__WEBPACK_IMPORTED_MODULE_0___default.a,\n  fetchItems: fetchItems\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (shop);\n\n//# sourceURL=webpack:///./src/components/shopPage/shop.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _components_headerPage_header__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/headerPage/header */ \"./src/components/headerPage/header.js\");\n/* harmony import */ var _components_shopPage_shop__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/shopPage/shop */ \"./src/components/shopPage/shop.js\");\n/* harmony import */ var _components_addPage_add__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/addPage/add */ \"./src/components/addPage/add.js\");\n/* harmony import */ var _modules_route__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modules/route */ \"./src/modules/route.js\");\n/* harmony import */ var _modules_indexed_bd__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modules/indexed-bd */ \"./src/modules/indexed-bd.js\");\n/* harmony import */ var _modules_online_check__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modules/online-check */ \"./src/modules/online-check.js\");\n\n\n\n\n\n\n\nif ('serviceWorker' in navigator) {\n  window.addEventListener('load', function () {\n    navigator.serviceWorker.register('/service-worker.js').then(function (registration) {\n      console.log('ServiceWorker registration successful with scope: ', registration.scope);\n    }, function (err) {\n      console.log('ServiceWorker registration failed: ', err);\n    });\n  });\n} // Check if user is online\n\n\nwindow.addEventListener('load', function () {\n  _modules_online_check__WEBPACK_IMPORTED_MODULE_5__[\"default\"].setConnectionState = navigator.onLine;\n  render();\n});\nwindow.addEventListener('online', function () {\n  _modules_online_check__WEBPACK_IMPORTED_MODULE_5__[\"default\"].setConnectionState = navigator.onLine; // POST, PUT, DELETE data to server when online === true\n\n  _modules_indexed_bd__WEBPACK_IMPORTED_MODULE_4__[\"default\"].updateServerResetLocal(); // all in one\n});\nwindow.addEventListener('offline', function () {\n  _modules_online_check__WEBPACK_IMPORTED_MODULE_5__[\"default\"].setConnectionState = navigator.onLine;\n}); // Navigate back and forth, re-renders components\n\nwindow.onpopstate = function () {\n  render();\n};\n\nvar render = function render(id) {\n  var app = document.getElementById(\"app\");\n\n  if (Object(_modules_route__WEBPACK_IMPORTED_MODULE_3__[\"default\"])() === '/index.html' || Object(_modules_route__WEBPACK_IMPORTED_MODULE_3__[\"default\"])() === '/' || Object(_modules_route__WEBPACK_IMPORTED_MODULE_3__[\"default\"])() === '/shop') {\n    app.innerHTML = _components_headerPage_header__WEBPACK_IMPORTED_MODULE_0__[\"default\"].headerComponent + _components_shopPage_shop__WEBPACK_IMPORTED_MODULE_1__[\"default\"].shopComponent;\n    _components_shopPage_shop__WEBPACK_IMPORTED_MODULE_1__[\"default\"].fetchItems('');\n  } else if (Object(_modules_route__WEBPACK_IMPORTED_MODULE_3__[\"default\"])() === '/cart') {\n    app.innerHTML = _components_headerPage_header__WEBPACK_IMPORTED_MODULE_0__[\"default\"].headerComponent + _components_shopPage_shop__WEBPACK_IMPORTED_MODULE_1__[\"default\"].shopComponent;\n    _components_shopPage_shop__WEBPACK_IMPORTED_MODULE_1__[\"default\"].fetchItems('?_limit=2');\n  } else if (Object(_modules_route__WEBPACK_IMPORTED_MODULE_3__[\"default\"])() === '/add-item') {\n    app.innerHTML = _components_headerPage_header__WEBPACK_IMPORTED_MODULE_0__[\"default\"].headerComponent + _components_addPage_add__WEBPACK_IMPORTED_MODULE_2__[\"default\"].addComponent;\n    _components_addPage_add__WEBPACK_IMPORTED_MODULE_2__[\"default\"].onLoadsubmitEvent();\n  } else if (Object(_modules_route__WEBPACK_IMPORTED_MODULE_3__[\"default\"])() === '/edit-item') {\n    app.innerHTML = _components_headerPage_header__WEBPACK_IMPORTED_MODULE_0__[\"default\"].headerComponent + _components_addPage_add__WEBPACK_IMPORTED_MODULE_2__[\"default\"].addComponent;\n    _components_addPage_add__WEBPACK_IMPORTED_MODULE_2__[\"default\"].onLoadsubmitEvent(id);\n  } else {\n    app.innerHTML = _components_headerPage_header__WEBPACK_IMPORTED_MODULE_0__[\"default\"].headerComponent + \"<div><h1 style=\\\"text-align:center;\\\">404</h1></div>\";\n  }\n\n  _components_headerPage_header__WEBPACK_IMPORTED_MODULE_0__[\"default\"].clickEvents();\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (render);\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ }),

/***/ "./src/modules/indexed-bd.js":
/*!***********************************!*\
  !*** ./src/modules/indexed-bd.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../index */ \"./src/index.js\");\n/* harmony import */ var _online_check__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./online-check */ \"./src/modules/online-check.js\");\n/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shared */ \"./src/modules/shared.js\");\nfunction _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\n\n\n\n!('indexedDB' in window) ? console.log('This browser doesn\\'t support IndexedDB') : console.log('This browser supports IndexedDB');\nvar idb = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;\nvar request = idb.open('shop', 1);\nvar shopObj = {\n  name: 'shop',\n  version: 1,\n  store: 'items'\n};\nvar deletedObj = {\n  name: 'store_deleted',\n  store: 'deleted_items'\n};\n\nfunction openTransaction(event, storeName) {\n  var db = event.target.result;\n  var transaction = db.transaction(storeName, 'readwrite');\n  return transaction.objectStore(storeName);\n}\n\nfunction CreateObjectStore(dbName, storeName) {\n  var request = idb.open(dbName);\n\n  request.onsuccess = function (event) {\n    var database = event.target.result;\n    database.close();\n    var secondRequest = idb.open(dbName, 2);\n\n    secondRequest.onupgradeneeded = function (event) {\n      var database = event.target.result;\n      var objectStore = database.createObjectStore(storeName, {\n        keyPath: 'id'\n      });\n    };\n\n    secondRequest.onsuccess = function (event) {\n      var deleteStore = openTransaction(event, 'deleted_items');\n      deleteStore.clear();\n    };\n  };\n}\n\nrequest.onerror = function (event) {\n  console.error(\"Database error: \" + event.target.errorCode);\n};\n\nrequest.onsuccess = function (event) {\n  var url = \"http://localhost:3000/items\";\n\n  if (_online_check__WEBPACK_IMPORTED_MODULE_1__[\"default\"].getConnectionState) {\n    _shared__WEBPACK_IMPORTED_MODULE_2__[\"default\"].fetchRequest(url, 'GET').then(function (items) {\n      var itemsStore = openTransaction(event, 'items');\n      itemsStore.clear();\n      items.forEach(function (item) {\n        itemsStore.add(item);\n      });\n      CreateObjectStore('store_deleted', 'deleted_items');\n    })[\"catch\"](function (err) {\n      return console.log(\"Fetch items for indexedDB: \".concat(err));\n    });\n  } else console.log('No internet connction...');\n};\n\nrequest.onupgradeneeded = function (event) {\n  var db = event.target.result;\n  var store = db.createObjectStore('items', {\n    keyPath: 'id'\n  });\n  store.createIndex('items_id_unique', 'id', {\n    unique: true\n  });\n};\n\nvar getLocalData = function getLocalData(id, selectDb) {\n  if (!('indexedDB' in window)) return null;\n  var request = idb.open(selectDb.name, selectDb.version);\n  return new Promise(function (resolve, reject) {\n    request.onsuccess = function (event) {\n      var store = openTransaction(event, selectDb.store);\n      !id ? resolve(store.getAll()) : resolve(store.get(Number(id)));\n    };\n  });\n};\n\nvar deleteLocalData = function deleteLocalData(id) {\n  var request = idb.open('shop', 1);\n\n  request.onsuccess = function () {\n    var itemsStore = openTransaction(event, 'items');\n    var itemPromise = new Promise(function (resolve, reject) {\n      resolve(itemsStore.get(Number(id)));\n    });\n    itemPromise.then(function (data) {\n      data.onsuccess = function () {\n        var itemObj = data.result;\n\n        if (!itemObj[\"new\"]) {\n          var secondRequest = idb.open('store_deleted');\n\n          secondRequest.onsuccess = function () {\n            var deletedId = Number(id);\n            var deletedStore = openTransaction(event, 'deleted_items');\n            deletedStore.add({\n              id: deletedId\n            });\n          };\n        }\n      };\n    });\n    itemsStore[\"delete\"](Number(id));\n  };\n};\n\nvar updateLocalData = function updateLocalData(obj, id) {\n  var newObj = _objectSpread({}, obj, {\n    id: id,\n    edited: true\n  });\n\n  var request = idb.open('shop', 1);\n\n  request.onsuccess = function () {\n    var itemsStore = openTransaction(event, 'items');\n    itemsStore.put(newObj);\n    alert('Item successfully edited...');\n  };\n};\n\nvar addLocalData = function addLocalData(obj) {\n  var id = Boolean;\n  var request = idb.open('shop', 1);\n\n  request.onsuccess = function (event) {\n    var itemsStore = openTransaction(event, 'items'); // Getting all keys => filter max index\n\n    var allKeys = itemsStore.getAllKeys();\n\n    allKeys.onsuccess = function () {\n      id = Math.max.apply(null, allKeys.result);\n\n      var newObj = _objectSpread({}, obj, {\n        id: ++id,\n        \"new\": true\n      });\n\n      itemsStore.add(newObj);\n    };\n\n    alert('Item successfully added...');\n  };\n}; // The offline => online UPDATE server, database...\n\n\nvar updateServerResetLocal = function updateServerResetLocal() {\n  console.log('POSTing new items to server, database...');\n  postNewItems().then(function (state) {\n    console.log('PUTing edited items to server, database...');\n    return state ? putEditedItems() : Promise.reject();\n  }).then(function (state) {\n    console.log('DELETING items from server, database...');\n    return state ? deleteServerItems() : Promise.reject();\n  }).then(function (state) {\n    if (state) {\n      resetLocalDatabase();\n      Object(_index__WEBPACK_IMPORTED_MODULE_0__[\"default\"])();\n    }\n  });\n};\n\nfunction postNewItems() {\n  return new Promise(function (resolve, reject) {\n    getLocalData(null, shopObj).then(function (data) {\n      data.onsuccess = function () {\n        var allItems = data.result;\n        var newItems = allItems.filter(function (item) {\n          return item[\"new\"];\n        });\n        if (newItems.length < 1) resolve(true);\n\n        var _loop = function _loop(index) {\n          setTimeout(function () {\n            delete newItems[index][\"new\"];\n            delete newItems[index].id;\n            if ('edited' in newItems[index]) delete newItems[index].edited;\n\n            var obj = _objectSpread({}, newItems[index]);\n\n            var url = 'http://localhost:3000/items';\n            _shared__WEBPACK_IMPORTED_MODULE_2__[\"default\"].fetchRequest(url, 'POST', obj).then(function (content) {\n              console.log(content);\n              if (index === newItems.length - 1) resolve(true);\n            })[\"catch\"](function (err) {\n              return console.log(\"Online post items: \".concat(err));\n            });\n          }, index * 1000);\n        };\n\n        for (var index = 0; index < newItems.length; index++) {\n          _loop(index);\n        }\n      };\n    });\n  });\n}\n\nfunction putEditedItems() {\n  return new Promise(function (resolve, reject) {\n    getLocalData(null, shopObj).then(function (data) {\n      data.onsuccess = function () {\n        var allItems = data.result;\n        var newItems = allItems.filter(function (item) {\n          return item.edited && !item[\"new\"];\n        });\n        if (newItems.length < 1) return resolve(true);\n\n        var _loop2 = function _loop2(index) {\n          setTimeout(function () {\n            var id = newItems[index].id;\n            delete newItems[index].edited;\n            if ('new' in newItems[index]) delete newItems[index][\"new\"];\n\n            var obj = _objectSpread({}, newItems[index]);\n\n            var url = \"http://localhost:3000/items/\".concat(id);\n            _shared__WEBPACK_IMPORTED_MODULE_2__[\"default\"].fetchRequest(url, 'PUT', obj).then(function (content) {\n              console.log(content);\n              if (index === newItems.length - 1) return resolve(true);\n            })[\"catch\"](function (err) {\n              return console.log(\"Online post items: \".concat(err));\n            });\n          }, index * 1000);\n        };\n\n        for (var index = 0; index < newItems.length; index++) {\n          _loop2(index);\n        }\n      };\n    });\n  });\n}\n\nfunction deleteServerItems() {\n  return new Promise(function (resolve, reject) {\n    getLocalData(null, deletedObj).then(function (data) {\n      data.onsuccess = function () {\n        var allIds = data.result;\n        if (allIds.length < 1) return resolve(true);\n\n        var _loop3 = function _loop3(index) {\n          setTimeout(function () {\n            var id = allIds[index].id;\n            var url = \"http://localhost:3000/items/\".concat(id);\n            _shared__WEBPACK_IMPORTED_MODULE_2__[\"default\"].fetchRequest(url, 'DELETE').then(function () {\n              console.log('Deleted from server, database...');\n              if (index === allIds.length - 1) return resolve(true);\n            })[\"catch\"](function (err) {\n              return console.log(\"Delete item: \".concat(err));\n            });\n          }, index * 1000);\n        };\n\n        for (var index = 0; index < allIds.length; index++) {\n          _loop3(index);\n        }\n      };\n    });\n  });\n}\n\nfunction resetLocalDatabase() {\n  var request = idb.open('shop', 1);\n\n  request.onsuccess = function (event) {\n    var url = \"http://localhost:3000/items\";\n    _shared__WEBPACK_IMPORTED_MODULE_2__[\"default\"].fetchRequest(url, 'GET').then(function (items) {\n      var itemsStore = openTransaction(event, 'items');\n      itemsStore.clear();\n      items.forEach(function (item) {\n        itemsStore.add(item);\n      });\n    })[\"catch\"](function (err) {\n      return console.log(\"Fetch items for indexedDB: \".concat(err));\n    });\n  };\n\n  var secondRequest = idb.open('store_deleted');\n\n  secondRequest.onsuccess = function (event) {\n    var deletedStore = openTransaction(event, 'deleted_items');\n    deletedStore.clear();\n  };\n}\n\nvar localDb = {\n  request: request,\n  getLocalData: getLocalData,\n  deleteLocalData: deleteLocalData,\n  updateLocalData: updateLocalData,\n  addLocalData: addLocalData,\n  updateServerResetLocal: updateServerResetLocal\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (localDb);\n\n//# sourceURL=webpack:///./src/modules/indexed-bd.js?");

/***/ }),

/***/ "./src/modules/online-check.js":
/*!*************************************!*\
  !*** ./src/modules/online-check.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nvar isOnline = {\n  onlineState: Boolean,\n\n  set setConnectionState(state) {\n    this.onlineState = state;\n  },\n\n  get getConnectionState() {\n    return this.onlineState;\n  }\n\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (isOnline);\n\n//# sourceURL=webpack:///./src/modules/online-check.js?");

/***/ }),

/***/ "./src/modules/route.js":
/*!******************************!*\
  !*** ./src/modules/route.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nvar route = function route() {\n  return location.pathname;\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (route);\n\n//# sourceURL=webpack:///./src/modules/route.js?");

/***/ }),

/***/ "./src/modules/shared.js":
/*!*******************************!*\
  !*** ./src/modules/shared.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../index */ \"./src/index.js\");\n\n\nvar changeLocation = function changeLocation(title, url, id) {\n  if (typeof history.pushState !== 'undefined') {\n    var obj = {\n      Title: title,\n      Url: url\n    };\n    history.pushState(obj, obj.Title, obj.Url);\n    Object(_index__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(id);\n  } else {\n    alert('Browser does not support HTML5 and I dont care...');\n  }\n};\n\nvar fetchRequest = function fetchRequest(url, method, obj) {\n  return fetch(url, {\n    method: method,\n    headers: {\n      'Accept': 'application/json',\n      'Content-Type': 'application/json'\n    },\n    body: JSON.stringify(obj)\n  }).then(function (resp) {\n    return resp.json();\n  })[\"catch\"](function (err) {\n    return console.log(err);\n  });\n};\n\nvar shared = {\n  changeLocation: changeLocation,\n  fetchRequest: fetchRequest\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (shared);\n\n//# sourceURL=webpack:///./src/modules/shared.js?");

/***/ })

/******/ });