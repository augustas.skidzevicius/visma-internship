import render from '../index';

const changeLocation = (title, url, id) => {
  if (typeof (history.pushState) !== 'undefined') {
    let obj = { Title: title, Url: url };
    history.pushState(obj, obj.Title, obj.Url);
    render(id);
  } else {
    alert('Browser does not support HTML5 and I dont care...');
  }
}

const fetchRequest = (url, method, obj) => {
  return fetch(url, {
    method,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(obj)
  })
    .then(resp => resp.json())
    .catch(err => console.log(err));
}

const shared = {
  changeLocation,
  fetchRequest
}

export default shared;
