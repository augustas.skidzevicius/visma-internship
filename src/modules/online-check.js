const isOnline = {
  onlineState: Boolean,
  set setConnectionState(state) {
    this.onlineState = state;
  },
  get getConnectionState() {
    return this.onlineState;
  }
}

export default isOnline;
