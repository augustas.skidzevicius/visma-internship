import render from '../index';
import isOnline from './online-check';
import shared from './shared';

(!('indexedDB' in window)) ? console.log('This browser doesn\'t support IndexedDB') : console.log('This browser supports IndexedDB');
const idb = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
const request = idb.open('shop', 1);

const shopObj = {
  name: 'shop',
  version: 1,
  store: 'items'
};

const deletedObj = {
  name: 'store_deleted',
  store: 'deleted_items'
};

function openTransaction(event, storeName) {
  const db = event.target.result;
  const transaction = db.transaction(storeName, 'readwrite');
  return transaction.objectStore(storeName);
}

function CreateObjectStore(dbName, storeName) {
  const request = idb.open(dbName);
  request.onsuccess = (event) => {
    const database = event.target.result;
    database.close();
    const secondRequest = idb.open(dbName, 2);
    secondRequest.onupgradeneeded = (event) => {
      const database = event.target.result;
      const objectStore = database.createObjectStore(storeName, { keyPath: 'id' });
    };
    secondRequest.onsuccess = (event) => {
      const deleteStore = openTransaction(event, 'deleted_items');
      deleteStore.clear();
    }
  }
}

request.onerror = (event) => {
  console.error("Database error: " + event.target.errorCode);
}

request.onsuccess = (event) => {
  const url = `http://localhost:3000/items`;
  if (isOnline.getConnectionState) {
    shared.fetchRequest(url, 'GET')
    .then(items => {
      const itemsStore = openTransaction(event, 'items');
      itemsStore.clear();
      items.forEach(item => {
        itemsStore.add(item);
      })
      CreateObjectStore('store_deleted', 'deleted_items');
    })
    .catch(err => console.log(`Fetch items for indexedDB: ${err}`));
  } else console.log('No internet connction...');
}

request.onupgradeneeded = (event) => {
  const db = event.target.result;
  const store = db.createObjectStore('items', { keyPath: 'id' });
  store.createIndex('items_id_unique', 'id', { unique: true });
}

const getLocalData = (id, selectDb) => {
  if (!('indexedDB' in window)) return null;

  const request = idb.open(selectDb.name, selectDb.version);
  return new Promise((resolve, reject) => {
    request.onsuccess = (event) => {
      const store = openTransaction(event, selectDb.store);
      !id ? resolve(store.getAll()) : resolve(store.get(Number(id)));
    }
  });
}

const deleteLocalData = (id) => {
  const request = idb.open('shop', 1);
  request.onsuccess = () => {
    const itemsStore = openTransaction(event, 'items');
    const itemPromise = new Promise((resolve, reject) => {
      resolve(itemsStore.get(Number(id)));
    });

    itemPromise.then(data => {
      data.onsuccess = () => {
        const itemObj = data.result;
        if (!itemObj.new) {
          const secondRequest = idb.open('store_deleted');
          secondRequest.onsuccess = () => {
            const deletedId = Number(id);
            const deletedStore = openTransaction(event, 'deleted_items');
            deletedStore.add({id: deletedId});
          }
        }
      }
    });

    itemsStore.delete(Number(id));
  }
}

const updateLocalData = (obj, id) => {
  const newObj = {
    ...obj,
    id,
    edited: true
  };

  const request = idb.open('shop', 1);
  request.onsuccess = () => {
    const itemsStore = openTransaction(event, 'items');
    itemsStore.put(newObj);
    alert('Item successfully edited...');
  }
}

const addLocalData = (obj) => {
  let id = Boolean;

  const request = idb.open('shop', 1);
  request.onsuccess = (event) => {
    const itemsStore = openTransaction(event, 'items');

    // Getting all keys => filter max index
    const allKeys = itemsStore.getAllKeys();
    allKeys.onsuccess = () => {
      id = Math.max.apply(null, allKeys.result);

      const newObj = {
        ...obj,
        id: ++id,
        new: true
      };
      itemsStore.add(newObj);
    }

    alert('Item successfully added...');
  }
}

// The offline => online UPDATE server, database...
const updateServerResetLocal = () => {
  console.log('POSTing new items to server, database...');
  postNewItems().then(state => {
    console.log('PUTing edited items to server, database...');
    return state ? putEditedItems() : Promise.reject();
  }).then(state => {
    console.log('DELETING items from server, database...');
    return state ? deleteServerItems() : Promise.reject();
  }).then(state => {
    if (state) {
      resetLocalDatabase();
      render();
    }
  });
}

function postNewItems() {
  return new Promise((resolve, reject) => {
    getLocalData(null, shopObj).then(data => {
      data.onsuccess = () => {
        const allItems = data.result;
        const newItems = allItems.filter(item => item.new);
        if (newItems.length < 1) resolve(true);

        for (let index = 0; index < newItems.length; index++) {
          setTimeout(() => {
            delete newItems[index].new;
            delete newItems[index].id;
            if (('edited' in newItems[index])) delete newItems[index].edited;
    
            const obj = {
              ...newItems[index]
            };

            const url = 'http://localhost:3000/items';
            shared.fetchRequest(url, 'POST', obj)
              .then(content => {
                console.log(content);
                if (index === newItems.length - 1) resolve(true);
              })
              .catch(err => console.log(`Online post items: ${err}`));
          }, index * 1000);
        }
      }
    });
  });
}

function putEditedItems() {
  return new Promise((resolve, reject) => {
    getLocalData(null, shopObj).then(data => {
      data.onsuccess = () => {
        const allItems = data.result;
        const newItems = allItems.filter(item => item.edited && !item.new);
        if (newItems.length < 1) return resolve(true);

        for (let index = 0; index < newItems.length; index++) {
          setTimeout(() => {
            const id = newItems[index].id;
            delete newItems[index].edited;
            if (('new' in newItems[index])) delete newItems[index].new;
    
            const obj = {
              ...newItems[index]
            };

            const url = `http://localhost:3000/items/${id}`;
            shared.fetchRequest(url, 'PUT', obj)
              .then(content => {
                console.log(content);
                if (index === newItems.length - 1) return resolve(true);
              })
              .catch(err => console.log(`Online post items: ${err}`));
          }, index * 1000);
        }
      }
    });
  });
}

function deleteServerItems() {
  return new Promise((resolve, reject) => {
    getLocalData(null, deletedObj).then(data => {
      data.onsuccess = () => {
        const allIds = data.result;
        if (allIds.length < 1) return resolve(true);
        
        for (let index = 0; index < allIds.length; index++) {
          setTimeout(() => {
            const id = allIds[index].id;
            const url = `http://localhost:3000/items/${id}`;
            shared.fetchRequest(url, 'DELETE')
              .then(() => {
                console.log('Deleted from server, database...');
                if (index === allIds.length - 1) return resolve(true);
              })
              .catch(err => console.log(`Delete item: ${err}`));
          }, index * 1000);
        }
      }
    });
  });
}

function resetLocalDatabase() {
  const request = idb.open('shop', 1);
  request.onsuccess = (event) => {
    const url = `http://localhost:3000/items`;
    shared.fetchRequest(url, 'GET')
      .then(items => {
        const itemsStore = openTransaction(event, 'items');
        itemsStore.clear();
        items.forEach(item => {
          itemsStore.add(item);
        });
      })
      .catch(err => console.log(`Fetch items for indexedDB: ${err}`));
  }

  const secondRequest = idb.open('store_deleted');
  secondRequest.onsuccess = (event) => {
    const deletedStore = openTransaction(event, 'deleted_items');
    deletedStore.clear();
  }
}

const localDb = {
  request,
  getLocalData,
  deleteLocalData,
  updateLocalData,
  addLocalData,
  updateServerResetLocal
}

export default localDb;
