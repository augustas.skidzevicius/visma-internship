import header from './components/headerPage/header';
import shop from './components/shopPage/shop';
import add from './components/addPage/add';
import route from './modules/route';
import localDB from './modules/indexed-bd';
import isOnline from './modules/online-check';

if ('serviceWorker' in navigator) {
  window.addEventListener('load', function() {
    navigator.serviceWorker.register('/service-worker.js').then(function(registration) {
      console.log('ServiceWorker registration successful with scope: ', registration.scope);
    }, function(err) {
      console.log('ServiceWorker registration failed: ', err);
    });
  });
}

// Check if user is online
window.addEventListener('load', () => { 
  isOnline.setConnectionState = navigator.onLine;
  render();
});
window.addEventListener('online', () => { 
  isOnline.setConnectionState = navigator.onLine;
  
  // POST, PUT, DELETE data to server when online === true
  localDB.updateServerResetLocal(); // all in one
});
window.addEventListener('offline', () => { isOnline.setConnectionState = navigator.onLine });

// Navigate back and forth, re-renders components
window.onpopstate = () => {
  render();
}

const render = function(id) {
  const app = document.getElementById("app");

  if (route() === '/index.html' || route() === '/' || route() === '/shop') {
    app.innerHTML = header.headerComponent + shop.shopComponent;
    shop.fetchItems('');
  } else if (route() === '/cart') {
    app.innerHTML = header.headerComponent + shop.shopComponent;
    shop.fetchItems('?_limit=2');
  } else if (route() === '/add-item') {
    app.innerHTML = header.headerComponent + add.addComponent;
    add.onLoadsubmitEvent();
  } else if (route() === '/edit-item') {
    app.innerHTML = header.headerComponent + add.addComponent;
    add.onLoadsubmitEvent(id);
  } else {
    app.innerHTML = header.headerComponent + `<div><h1 style="text-align:center;">404</h1></div>`;
  }

  header.clickEvents();
}

export default render;
