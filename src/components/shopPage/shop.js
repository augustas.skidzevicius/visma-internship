import shopComponent from './shop.html';

import shared from '../../modules/shared';
import localDb from '../../modules/indexed-bd';
import isOnline from '../../modules/online-check';

import ninjaShirt from '../../assets/images/ninja-shirt.jpg';
import ninjaSweater from '../../assets/images/ninja-sweater.jpg';

const shopObj = {
  name: 'shop',
  version: 1,
  store: 'items'
};

function domItem(item) {
  return `
    <figure class="item ${(item.discount) ? 'sale' : ''}">
      <img src="${selectImage(item.image)}" alt="happy ninja" width="100%">
      <button class="btn-edit" data-item="${item.id}" type="button">EDIT</button>
      <div class="info">
        <h4 class="truncate">${item.name}</h4>
        <strong>${(item.discount) ? `<span>${(item.price + Number(item.discount)).toFixed(2)}</span>` : ''} ${item.price.toFixed(2)}</strong>
        <button class="btn-add" data-item="${item.id}" type="button">ADD TO CARD</button>
      </div>
    </figure>
  `;
}

function selectImage(img) {
  switch(img) {
    case 'ninja-shirt':
      return ninjaShirt;
    case 'ninja-sweater':
      return ninjaSweater;
  }
}

const fetchItems = (query) => {
  if (isOnline.getConnectionState) {
    const url = `http://localhost:3000/items${query}`;
    shared.fetchRequest(url, 'GET')
      .then(items => {
        generateItems(items);
        addEventToBtns();
      })
      .catch(err => console.log(`Fetch items. Maybe offline?: ${err}`));
  } else {
    localDb.getLocalData(null, shopObj).then(data => {
      data.onsuccess = () => {
        console.log('Adding items form indexdeDB');
        generateItems(data.result);
        addEventToBtns();
      }
    })
  }
  
}

 function generateItems(items) {
  const itemsElement = document.querySelector('.items');
  Array.prototype.forEach.call(items, item => {
    itemsElement.innerHTML += domItem(item);
  });
}

function addEventToBtns() {
  const btnsEdit = document.querySelectorAll('.btn-edit');
  Array.prototype.forEach.call(btnsEdit, btn => {
    btn.addEventListener('click', goToEdit);
  });

  const btnNew = document.querySelector('.btn-new');
  btnNew.addEventListener('click', () => {
    goToAddPage(undefined);
  });

  const btnsNodeList = document.querySelectorAll('.btn-add');
  Array.prototype.forEach.call(btnsNodeList, btn => {
    btn.addEventListener('click', addToCart);
  });
}

function goToAddPage(id) {
  id = (id !== undefined) 
    ? shared.changeLocation('/edit-item' ,'/edit-item', id) 
    : shared.changeLocation('/add-item' ,'/add-item');
}

function goToEdit(event) {
  const id = event.target.dataset.item;
  goToAddPage(id);
}

function addToCart(event) {
  console.log(event.target.dataset.item);
}

const shop = {
  shopComponent,
  fetchItems
}

export default shop;
