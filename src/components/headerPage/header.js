import headerComponent from './header.html';
import shared from '../../modules/shared';

const clickEvents = function() {
  const btnShop = document.querySelector('.btn-shop');
  const btnCart = document.querySelector('.btn-cart');

  btnShop.addEventListener('click', () => {
    shared.changeLocation('shop' ,'/shop');
  });

  btnCart.addEventListener('click', () => {
    shared.changeLocation('cart', '/cart');
  });
}

const header = {
  headerComponent,
  clickEvents
}

export default header;
