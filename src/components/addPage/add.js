import addComponent from './add.html';
import shared from '../../modules/shared';
import isOnline from '../../modules/online-check';
import localDB from '../../modules/indexed-bd';

const shopObj = {
  name: 'shop',
  version: 1,
  store: 'items'
}

function onSubmit(method, id) {
  let validName = false;
  let validPrice = false;
  let validImage = false;
  let item = {
    name: '',
    price: 0,
    discount: null,
    image: ''
  };

  Array.prototype.forEach.call(formElement(), input => {
    if (Object.keys(item).includes(input.name)) {
      if (input.name === 'price') {
        item[input.name] = Number(input.value);
        if (input.value.length > 0) {
          validPrice = true;
        }
      } else if (input.name === 'discount') {
        item[input.name] = (document.querySelector('.form-sale').checked) ? input.value : null;
      } else {
        item[input.name] = input.value;
        if (input.name === 'name') {
          if (input.value.length > 0) {
            validName = true;
          }
        }
        if (input.name === 'image') {
          if (input.value.length !== 0) {
            validImage = true;
          }
        }
      }
    }
  });

  if (!isOnline.getConnectionState && id) {
    localDB.getLocalData(id, shopObj).then(data => {
      data.onsuccess = () => {
        item = {
          ...item,
          new: data.result['new']
        }

        if (validName && validPrice && validImage) {
          (method === 'post') ? postItem(item) : putItem(item, id);
        } else alert('Name, price, image fields must be filled...');
      }
    });
  } else if (validName && validPrice && validImage) {
    (method === 'post') ? postItem(item) : putItem(item, id);
  } else alert('Name, price, image fields must be filled...');
}

function onEditRender(content) {
  const obj = content;
  Array.prototype.forEach.call(formElement(), input => {
    if (Object.keys(obj).includes(input.name)) {
      input.value = obj[input.name];
    }
    if (obj.discount) {
      document.querySelector('.form-sale').checked = true;
    }
  });
  
  const btnDelete = document.querySelector('.btn-delete-item');
  btnDelete.classList.remove('hidden');
  btnDelete.addEventListener('click', () => {
    deleteItem(btnDelete, obj.id);
  });
}

function onEditFetch(id) {
  if (isOnline.getConnectionState) {
    const url = `http://localhost:3000/items?id=${id}`;
    shared.fetchRequest(url, 'GET')
      .then(content => onEditRender(content[0]))
      .catch(err => console.log(`Item edit: ${err}`));
  } else {
    localDB.getLocalData(id, shopObj).then(data => {
      data.onsuccess = () => {
        console.log('Getting item from indexedDB');
        onEditRender(data.result);
      }
    });
  }
}

function deleteItem(el, id) {
  if (isOnline.getConnectionState) {
    const url = `http://localhost:3000/items/${id}`;
    shared.fetchRequest(url, 'DELETE')
      .then(() => {
        el.classList.add('hidden');
        alert("Item successfully deleted...");
        shared.changeLocation('/shop', '/shop');
      })
      .catch(err => console.log(`Delete item: ${err}`));
  } else {
    localDB.deleteLocalData(id);
    alert("Item successfully deleted...");
    el.classList.add('hidden');
    shared.changeLocation('/shop', '/shop');
  }
}

function putItem(obj, id) {
  if (isOnline.getConnectionState) {
    const url = `http://localhost:3000/items/${id}`;
    shared.fetchRequest(url, 'PUT', obj)
      .then(content => {
        console.log(content);
        alert('Item successfully edited...');
      })
      .catch(err => console.log(`Put Item: ${err}`));
  } else {
    localDB.updateLocalData(obj, Number(id));
  }
}

function postItem(obj) {
  if (isOnline.getConnectionState) {
    const url = 'http://localhost:3000/items';
    shared.fetchRequest(url, 'POST', obj)
      .then(content => {
        console.log(content);
        alert('Item successfully added...');

        // Clear form inputs
        document.querySelector('.form-sale').checked = false;
        Array.prototype.forEach.call(formElement(), input => {
          (input.type !== 'select-one') ? input.value = '' : null;
        });
      })
      .catch(err => console.log(`Post item: ${err}`));
  } else {
    localDB.addLocalData(obj);

    // Clear form inputs
    document.querySelector('.form-sale').checked = false;
    Array.prototype.forEach.call(formElement(), input => {
      (input.type !== 'select-one') ? input.value = '' : null;
    });
  }
}

function formElement() {
  return [document.querySelector('.form-name'), document.querySelector('.form-price'), document.querySelector('.form-discount'), document.querySelector('.form-img')];
}

function formType() {
  const pageTitle = document.querySelector('.page-title');
  const btnAddEdit = document.querySelector('.btn-add-item');
  if (history.state.Title === '/add-item') {
    pageTitle.innerHTML = 'Add Item';
    btnAddEdit.innerHTML = 'Add Item';
  } else {
    pageTitle.innerHTML = 'Edit Item';
    btnAddEdit.innerHTML = 'Edit Item';
  }
}

const onLoadsubmitEvent = (id) => {
  formType();
  const btnAddItem = document.querySelector('.btn-add-item');
  if (id !== undefined && id) {
    onEditFetch(id);
    btnAddItem.addEventListener('click', () => {
      onSubmit('put', id);
    });
  } else {
    btnAddItem.addEventListener('click', () => {
      onSubmit('post');
    });
  }
}

const add = {
  addComponent,
  onLoadsubmitEvent
}

export default add;
